### Install Docker

From [https://docs.docker.com/install/linux/docker-ce/centos/](https://docs.docker.com/install/linux/docker-ce/centos/)

##### SET UP THE REPOSITORY
* Install required packages
```
$ sudo yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2

```
* Set up stable repository
```
$ sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

```

##### INSTALL DOCKER CE

* Install Docker CE

```
$ sudo yum install docker-ce docker-ce-cli containerd.io

```
* Start Docker (if necessary)
```
$ sudo systemctl start docker
$ sudo systemctl enable --now docker

```

* Test if Docker is running
```
$ docker ps 

```

* Add user to group **docker**
```
$ sudo usermod -aG docker osboxes

```
Don't forget to log out and log in again.

