# Docker Fundamentals

#### Slides
https://bitbucket.org/pgazda/docker-fundamentals/downloads/docker-fundamentals-slides.pdf

#### Excersizes

https://bitbucket.org/pgazda/docker-fundamentals/downloads/docker-fundamentals-exercises.pdf