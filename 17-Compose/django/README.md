* Create Django project
```
sudo docker-compose run web django-admin startproject composeexample .

```
* List created files and change owner if necessary
```
ls -l
sudo chown -R osboxes:osboxes .

```
* Modify settings.py for DB connection
```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'HOST': 'db',
        'PORT': 5432,
    }
}

```
