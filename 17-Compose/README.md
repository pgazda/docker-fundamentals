### Install Docker Compose

From [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

* Download latest version of Docker Compose:
```
$ sudo curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

```
* Apple executable permissions 
```
$ sudo chmod +x /usr/local/bin/docker-compose

```
* Test the installation
```
$ docker-compose --version

```
  
### Get Dockercoins App
* Download from github
```
$ git clone -b 17.06 https://github.com/docker-training/orchestration-workshop.git

```

###
* Add EPEL repository for CentOS
```
sudo yum install epel-release
sudo yum repolist

```

